# Timber_ohos

**本项目是基于开源项目 Timber 进行鸿蒙化的移植和开发的，可以通过项目标签以及github地址（ https://github.com/JakeWharton/timber ）追踪到原安卓项目版本**

#### 项目介绍

- 项目名称：Hilog实用扩充项目
- 所属系列：鸿蒙的第三方组件适配移植
- 功能：增强鸿蒙输出日志的能力
- 项目移植状态：完成
- 调用差异：无
- 开发版本：sdk5，DevEco Studio2.1 beta3
- 项目作者和维护人：陈丛笑
- 邮箱：isrc_hm@iscas.ac.cn
- 原项目Doc地址：https://github.com/JakeWharton/timber

#### 项目介绍

- 编程语言：Java 
- 外部库依赖：org.jar
Timber是具有小型可扩展API的记录器，可在鸿蒙常规HiLog类之上提供实用程序。


#### 安装教程

1. 下载Timber的jar包Timber.jar。
2. 启动 DevEco Studio，将下载的jar包，导入工程目录“entry->libs”下。



3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下jar包的引用。
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```
4. 在导入的jar包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

在sdk5，DevEco Studio2.1 beta3下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

1. 使用前需要安装Tree实例，可以通过Timber.plant函数来安装实例. Tree的安装应该尽早，在大多数情况下可以安装在应用初始化的时候。

```
Timber.plant(new Timber.DebugTree());
```
2.使用实例
```
Timber.e  ("Timber.e 测试成功！！！");
Timber.d  ("Timber.d 测试成功！！！");
Timber.i  ("Timber.i 测试成功！！！");
Timber.v   ("Timber.v 测试成功！！！");
Timber.w   ("Timber.w 测试成功！！！");
Timber.wtf   ("Timber.wtf测试成功！！！");
```

#### 版本迭代


- v0.1.0-alpha


#### 版权和许可信息
- Apache Licence


