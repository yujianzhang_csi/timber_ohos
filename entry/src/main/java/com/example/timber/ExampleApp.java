package com.example.timber;

import ohos.aafwk.ability.AbilityPackage;
import ohos.hiviewdfx.HiLog;
import timber.log.Timber;


public class ExampleApp extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree(0x001f00));
        } else {
            Timber.plant(new CrashReportingTree());
        }
    }
    private static class CrashReportingTree extends Timber.Tree {
        @Override protected void log(int priority, String tag,  String message, Throwable t) {
            if (priority == HiLog.INFO || priority == HiLog.DEBUG) {
                return;
            }

            FakeCrashLibrary.log(priority, tag, message);

            if (t != null) {
                if (priority == HiLog.ERROR) {
                    FakeCrashLibrary.logError(t);
                } else if (priority == HiLog.WARN) {
                    FakeCrashLibrary.logWarning(t);
                }
            }
        }
    }
}
