package com.example.timber.slice;

import com.example.timber.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import timber.log.Timber;


public class MainAbilitySlice extends AbilitySlice {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        Timber.tag("LifeCycles");
        Timber.d("Timber 测试成功！！！");

        findComponentById(ResourceTable.Id_btn1).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                // 按钮被点击
                // 按钮被按下后，需要执行的操作

                Timber.e("Timber.e 测试成功！！！");
                Timber.d("Timber.d 测试成功！！！");
                Timber.i("Timber.i 测试成功！！！");

                Timber.v("Timber.v 测试成功！！！");
                Timber.w("Timber.w 测试成功！！！");

                Timber.wtf("Timber.wtf测试成功！！！");
            }
        });
    }

}
